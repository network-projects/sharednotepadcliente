from socket import *


class ClienteSocket:

    def __init__(self):
        self.sockobj = socket(AF_INET, SOCK_STREAM)
        print("tudo pronto")

    def conectar(self, host, porta):
        self.host = host
        self.porta = porta
        print(self.host, self.porta)
        self.sockobj.connect((self.host, self.porta))

    def enviar(self, mensagem):

        print('enviar' + mensagem)
        self.sockobj.sendall(mensagem.encode())


    def receber(self):
        mensagem = self.sockobj.recv(4096)
        return mensagem
